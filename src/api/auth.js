import { Simulate } from "react-dom/test-utils";

const registerUrl = 'http://localhost:4000/users/register'; 
const checkSessionUrl = 'http://localhost:4000/users/check-session';
const loginUrl = 'http://localhost:4000/users/login';
const logoutUrl = 'http://localhost:4000/users/logout';

export const register = async (userData) => {
  console.log('datos-usuario', userData); 
  const response = await fetch(registerUrl, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json', 
      'Access-Control-Allow-Origin': '*',
    },
    credentials: 'include',
    body: JSON.stringify(userData),
  });

  console.log('consulta enviada');
  const jsonResponse = await response.json(); 

  // Si hay un error en la respuesta, devolveremos el error del servidor que tenemos en .message
  /*if (!response.ok) {
    throw new Error(jsonResponse.message);
  } */
  console.log('REGISTER', jsonResponse);  

  // Si tenemos una respuesta adecuada, devolvemos el resultado que habrá en .data
  return jsonResponse; 
};

export const login = async (userData) => {
  const response = await fetch(loginUrl, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    },
    credentials: 'include',
    body: JSON.stringify(userData),
  });

  const jsonResponse = await response.json();
  console.log('LOGIN', jsonResponse);
  // Si hay un error en la respuesta, devolveremos el error del servidor que tenemos en .message
  /*if (!response.ok) {
    throw new Error(jsonResponse.message);
  } */

  // Si tenemos una respuesta adecuada, devolvemos el resultado que habrá en .data
  return jsonResponse;
};

export const checkSession = async () => {
  const response = await fetch(checkSessionUrl, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    },
    credentials: 'include',
  });

  const jsonResponse = await response.json();
  console.log('CHECK', jsonResponse);
  // Si hay un error en la respuesta, devolveremos el error del servidor que tenemos en .message
  if (!response.ok) {
    throw new Error(jsonResponse.message);
  }

  // Si tenemos una respuesta adecuada, devolvemos el resultado que habrá en .data
  return jsonResponse;
};

export const logout = async () => {
  const response = await fetch(logoutUrl, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    credentials: 'include',
  });

  const jsonResponse = await response.json();

  // Si hay un error en la respuesta, devolveremos el error del servidor que tenemos en .message
  if (!response.ok) {
    throw new Error(jsonResponse.message);
  }

  // Si tenemos una respuesta adecuada, devolvemos el resultado que habrá en .data
  return jsonResponse;
};