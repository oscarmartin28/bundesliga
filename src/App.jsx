import './App.scss';
import React, {useEffect, useState} from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

// COMPONENTES

import Home from './components/Home';
import Clasificacion from './components/Clasificacion';
import Equipos from './components/Equipos';
import News from './components/News';
import Plantilla from './components/Plantilla';

function App() {

  function sortTeams(array) {
    array.sort((a, b) => a.position - b.position);
    console.log('ORDENAMELO', array);
  };

  const [playerList, setPlayerList] = useState([]);

  useEffect(() => {
    fetch('http://localhost:4000/players/')
    .then((res) => res.json())
    .then((data) => {
      console.log('datos', data);
      setPlayerList(data); 
    });
  }, []);

  const [teamList, setTeamList] = useState([]);

  useEffect(() => {
    fetch('http://localhost:4000/teams/')
    .then((res) => res.json())
    .then((data) => {
      sortTeams(data);
      console.log('datos', data);
      setTeamList(data); 
    });
  }, []);

  return (
    <Router>
      <div className="App">
        <Switch> 
          <Route path="/plantilla/:id" exact render={(props) => <Plantilla {...props} teamList={teamList} />} />
          <Route path="/news" exact component={News} />
          <Route path="/equipos" exact render={(props) => <Equipos {...props}  teamList={teamList} />} />
          <Route path="/clasificacion" exact render={(props) => <Clasificacion {...props} teamList={teamList} />} />
          <Route path="/" component={Home} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
