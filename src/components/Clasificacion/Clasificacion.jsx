import React from 'react';

import './Clasificacion.scss';
import Header from '../Header';
import Footer from '../Footer';

function Clasificacion(props) {

    console.log('PROPS', props.teamList);

    return (
        <div className="clasificacion">
            <Header />
            <div className="container">
            {props.teamList.length ? (
                <table className="table">
                <tbody>
                    <tr className="table__tr">
                        <th className="table__th">Posición</th>
                        <th className="table__th">Equipo</th>
                        <th className="table__th">Partidos</th>
                        <th className="table__th">Puntos</th>
                    </tr>
                    {props.teamList.map((team) => {
                        return (
                            <tr key={team.name} className="table__tr">
                                <td className="table__td">{team.position}</td>
                                <td className="table__td"><img className="table__img" src={team.picture} alt=""/>{team.name}</td>
                                <td className="table__td">{team.partidos}</td>
                                <td className="table__td">{team.puntos}</td>
                            </tr>
                        )
                    })}
                </tbody>
                </table>
            ) : <p className="loading">Cargando la clasificación...</p>}
            </div>
            <Footer />
        </div>
    );
  }

export default Clasificacion;