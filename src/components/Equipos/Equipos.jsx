import React from 'react';

import './Equipos.scss';

import Header from '../Header';
import Footer from '../Footer';
import Card from '../Card';

class Equipos extends React.Component {

	render() {
        const {teamList} = this.props;
        console.log('equipos', teamList);

		return (
		  <div className="teams">
            <Header />
            <div className="teams-box">
            {teamList.length ? (
                <>
                {teamList.map((team) => {
                    return(
                        <Card key={team.name} team={team} />
                    )
                })}
                </>
            ) : <p className="loading">Cargando los equipos...</p>}
            </div>
            <Footer />
		  </div>
		);
	}
}

export default Equipos;