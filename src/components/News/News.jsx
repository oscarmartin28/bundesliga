import React from 'react';

import './News.scss';
import Header from '../Header';
import Footer from '../Footer';

function News() {
    return (
        <div className="noticias">
            <Header />
            <div className="news">
                <div className="news-box">

                    <h1 className="news__encabezado">EL DORTMUND BURLA EL DESTINO DEL LEVERKUSEN, QUE FUE ELIMINADO EN LA PRÓRROGA.</h1>

                    <div className="news__foto">
                        <img className="foto" src="https://storage.googleapis.com/afs-prod/media/ec4de7b3b7024aadbd56fde6a301d303/2334.jpeg" alt="Goles de Haaland, Can y Sancho" />
                    </div>

                    <p className="news__description">
                        Un gol de Erling Haaland en la prórroga dio el triunfo al Borussia Dortmund por 3-2 ante el Paderborn y con ello el pase a cuartos de final de la Copa de Alemania en un partido dramático. El equipo de la cuenca del Ruhr, rival del Sevilla en la Champions League, se adelantó 2-0 en el luminoso sobre el cuarto de hora con goles de Can y Sancho.
                    </p>

                    <div className="box-description2">
                        <img className="description__img" src="https://e00-marca.uecdn.es/assets/multimedia/imagenes/2021/02/02/16123055759077.jpg" alt="" />
                        <p className="news__description">Pero el Paderborn, conjunto de la segunda división germana, no dio su brazo a torcer. Justvan recortó distancias en el 79’ y Owuso empató en un dramático último minuto del tiempo añadido.</p>
                    </div>

                    <p className="news__description">
                        Haaland marcó el 3-1, pero la jugada se había originado en un penalti cometido en el área del Dortmund y el VAR actúo para precipitar la igualada. Repitió el noruego en la prórroga y esta vez sí valió, si bien el Paderborn tuvo en el último instante un par de ocasiones para llevar el duelo a los penaltis.  
                    </p>
                </div>
            </div>
            <Footer />
        </div>
    );
  }

export default News;