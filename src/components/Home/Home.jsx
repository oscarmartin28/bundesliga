import React from 'react';

import './Home.scss';
import Register from '../../containers/Register';
import Login from '../../containers/Login';
import Footer from '../Footer';
import Header from '../Header';

function Home() {
    return (
        <div className="home">
            <Header />
            <div className="box">
                <div className="box-form">
                    <Register />
                    <Login />
                </div>

                <div className="box-img">
                    <img className="box__img" src="https://upload.wikimedia.org/wikipedia/commons/d/d5/Austrian_Football_Bundesliga_Logo.png" alt=""></img>
                </div>

                <div className="box-form"></div>
            </div>
            <Footer />
        </div>
    );
  }

export default Home;