import React from 'react';
import './Footer.scss';

function Footer() {
    return (
        <div className="footer">
        <div className="logo"></div>

        <div className="copyright">
            &copy; 2021 Óscar Martín-Hervás Ruiz
        </div>

        <div className="icons">
            <span className="icon-phone icon">  689349901</span>
            <span className="icon-mail4 icon">  oscarmartin00@gmail.com</span>
            <span className="icon-github icon">  oscarmartin28</span>
        </div>
        </div>
    );
  }

export default Footer;