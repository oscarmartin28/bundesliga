import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import './Card.scss';

class Card extends React.Component {

  state = {
    team: this.props.team
  }

	render() {

        const {team} = this.props;

		return (
		  <div className="card">
            <h2 className="card__name">{team.name}</h2>
            <div className="box-logo">
                <img className="card__logo" src={team.picture} alt=""></img>
            </div>
            <h4 className="card__detalle">Posición en liga: {team.position}</h4>
            <h4 className="card__detalle">Puntos: {team.puntos}</h4>
            <button onClick={() => this.props.history.push(`/plantilla/${team._id}`)} className="card__button"><Link className="link" to="/plantilla">Plantilla</Link></button>
		  </div>
		);
	}
}

export default withRouter(Card);