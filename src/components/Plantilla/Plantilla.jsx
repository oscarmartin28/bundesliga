import React from 'react';

import './Plantilla.scss';

import Header from '../Header';
import Footer from '../Footer';

class Plantilla extends React.Component {

state = {
    team: ''
}
    componentDidMount() {
        console.log('PROPS-PLANTILLA', this.props);
        let team = this.props.teamList.filter(team => {
            return this.props.match.params.id===team._id;
        })[0]
        console.log(team);
        this.setState({
            team: team
        })
    }

	render() {

        const {team} = this.state;
        //console.log('BURKI', team);
        //console.log('team-length', team.length);

		return (
		  <div className="plantilla">
            <Header />
            <div className="plantilla-box">
            

                {team && team.players && team.players.map((player) => { return (
                <div className="card">
                    <h2 className="card__name">{player.name}</h2>
                    <div className="box-logo">
                        <img className="card__logo" src={player.picture} alt=""></img>
                    </div>
                    <h4 className="card__detalle">Posición: {player.position}</h4>
                    <h4 className="card__detalle">Edad: {player.age}</h4>
                    <h4 className="card__detalle">#: {player.number}</h4>
		        </div>
                )})
                }


            </div>
            <Footer />
		  </div>
		);
	}
}

export default Plantilla;