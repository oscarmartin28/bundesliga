import React from 'react';
import {Link} from 'react-router-dom';
import './Header.scss';
import { logout } from '../../api/auth';

function Header(props) {

  const handleOnClickLogout = async () => {
    await logout();
    props.handleLogout();
  };

    return (
      <header className="header">
          <div className="logo">
              <img src="./img/logo-bundesliga.png" alt="" className="logo__img"/>
              <h1 className="logo__titulo">BUNDESLIGA</h1>
          </div>

          <div className="menu">
            <ul className="menu__ul">
                <li className="menu__li"><Link className="link" to="/">HOME</Link></li>
                <li className="menu__li"><Link className="link" to="/clasificacion">CLASIFICACIÓN</Link></li>
                <li className="menu__li"><Link className="link" to="/equipos">EQUIPOS</Link></li>
                <li className="menu__li"><Link className="link" to="/news">ULTIMAS NOTICIAS</Link></li>
                
            </ul>
          </div>
      </header>
    );
  }

export default Header;