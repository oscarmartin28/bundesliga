import React, { Component } from 'react';
import './Register.scss';
import { register } from '../../api/auth';

class Register extends Component {
  state = {
    form: {
      email: '',
      password: '',
    },
    error: null,
  };

  handleChangeInput = (e) => {
    const inputName = e.target.name;
    const inputValue = e.target.value;

    // Necesitamos mantener la información previa en el formulario al tener una clave de profundidad.
    this.setState((prevState) => ({
      form: {
        ...prevState.form,
        [inputName]: inputValue,
      },
    }));
  };

  handleFormSubmit = async (e) => {
    e.preventDefault();

    try {
      // Envíamos como argumento el estado actual del formulario
      const data = await register(this.state.form);
			// Por ahora dejaremos un log y más tarde añadiremos más lógica
      console.log('¡Registro completado!', data);
    } catch (err) {
      this.setState({
        error: err.message,
      });
    }
  };

  render() {
    return (
        
      <form className="form" onSubmit={this.handleFormSubmit}>
        <h3 className="titulo">Registrate</h3>

        <label className="form__label" htmlFor="email">
          <p>Email</p>
          <input
            className="form__input"
            type="text"
            name="email"
            value={this.state.email}
            onChange={this.handleChangeInput}
          />
        </label>

        <label className="form__label" htmlFor="password">
          <p>Contraseña</p>
          <input
            className="form__input"
            type="password"
            name="password"
            value={this.state.password}
            onChange={this.handleChangeInput}
          />
        </label>

        {this.state.error ? (
          <p style={{ color: 'red' }}>{this.state.error}</p>
        ) : null}

        <button className="form__button" type="submit">Registrarse</button>
      </form>
    );
  }
}

export default Register;